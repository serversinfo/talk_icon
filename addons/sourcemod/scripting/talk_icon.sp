#pragma semicolon 1
#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <voiceannounce_ex>
#include <basecomm>

#define PLUGIN_VERSION "1.1.0.0"
public Plugin:myinfo =
{
	name 		= "Talk Icon",
	author 		= "AlexTheRegent",
	description = "",
	version 	= PLUGIN_VERSION,
	url 		= ""
}

new Handle:	g_hTrie_iSpriteToClient;
bool		g_bIsTalking[MAXPLAYERS+1];
new 		g_iSprites[MAXPLAYERS+1];
new String:TalkIconPath[PLATFORM_MAX_PATH] = "particle/voice_icon_particle.vmt";

public OnPluginStart()
{
	g_hTrie_iSpriteToClient = CreateTrie();

	HookEvent("player_spawn", Ev_PlayerSpawn);
	HookEvent("player_death", Ev_PlayerDeath);
}

public OnMapStart() 
{
	PrecacheModel(TalkIconPath);
}

public OnClientDisconnect(client)
{
	RemoveSprite(client);
	PrintToConsole(client, "OnClientDisconnect RemoveSprite %d", client);
}

public bool OnClientSpeakingEx(client)
{
	if(BaseComm_IsClientMuted(client))
			return false;
		
	if ( IsPlayerAlive(client) && !g_bIsTalking[client] )
	{
		RemoveSprite(client);
		AttachSprite(client);
		PrintToConsole(client, "OnClientSpeakingEx(%d) call Remove and attach", client);
	}
	
	g_bIsTalking[client] = true;
	return false;
}

public OnClientSpeakingEnd(client)
{
	RemoveSprite(client);
	g_bIsTalking[client] = false;
}

AttachSprite(client)
{
	
	new entity = CreateEntityByName("env_sprite_oriented");
	
		if (entity > 0 && IsValidEntity(entity) && client > 0)
		{
			decl String:Buffer[64];
			DispatchKeyValue(entity, "classname", "env_sprite_oriented");
			DispatchKeyValue(entity, "spawnflags", "1");
			DispatchKeyValue(entity, "scale", "0.25");
			DispatchKeyValue(entity, "rendermode", "1");
			DispatchKeyValue(entity, "rendercolor", "0 255 0");
			DispatchKeyValue(entity, "model", TalkIconPath);
			Format(Buffer, sizeof(Buffer), "Client%d", client);						// добавил
			DispatchKeyValue(client, "targetname", Buffer);						// добавил
			DispatchKeyValue(entity, "parentname", Buffer);						// добавил
			DispatchSpawn(entity);

			decl Float:pos[3];
			GetClientAbsOrigin(client, pos);
			pos[2] += 80.0;
			TeleportEntity(entity, pos, NULL_VECTOR, NULL_VECTOR);

			//SetVariantString("!activator");
			//AcceptEntityInput(entity, "SetParent", client, entity, 0);			//чтобы самому не видеть спрей
			SetVariantString(Buffer);
			AcceptEntityInput(entity, "SetParent");			//чтобы самому не видеть спрей
			SetVariantString("!activator");
			AcceptEntityInput( entity, "SetParentAttachment" ); 
			SetEntPropEnt(entity, Prop_Send, "m_hOwnerEntity", client);				//добавил
			g_iSprites[client] = entity;
			
			decl String:sEntity[8]; IntToString(entity, sEntity, sizeof(sEntity));
			SetTrieValue(g_hTrie_iSpriteToClient, sEntity, GetClientTeam(client));
			SDKHook(entity, SDKHook_SetTransmit, OnSetTransmit);
		
			return entity;
		}
	
	return -1;
}
	
public Action:OnSetTransmit(entity, client)
{
	decl String:sEntity[8], team; IntToString(entity, sEntity, sizeof(sEntity));
	if ( GetTrieValue(g_hTrie_iSpriteToClient, sEntity, team) ) {
		if ( team == 1 || team == GetClientTeam(client) ) {
			return Plugin_Stop;
		}
	}
	return Plugin_Continue;
}

RemoveSprite(client)
{
	if ( g_iSprites[client] && IsValidEdict(g_iSprites[client]) ) {
		AcceptEntityInput(g_iSprites[client], "kill");
		RemoveEdict(g_iSprites[client]);
	}
}

public Ev_PlayerSpawn(Handle:hEvent, const String:sEvName[], bool:bSilent)
{
	new iClient = GetClientOfUserId(GetEventInt(hEvent, "userid"));
	if ( g_bIsTalking[iClient] ) {
		RemoveSprite(iClient);
		AttachSprite(iClient);
	}
	PrintToConsole(iClient, "Ev_PlayerSpawn RemoveSprite AttachSprite (%d)", iClient);
	
}

public Ev_PlayerDeath(Handle:hEvent, const String:sEvName[], bool:bSilent)
{
	new iClient = GetClientOfUserId(GetEventInt(hEvent, "userid"));
	if ( g_bIsTalking[iClient] ) {
		RemoveSprite(iClient);
	}
	PrintToConsole(iClient, "Ev_PlayerDeath RemoveSprite %d", iClient);
}